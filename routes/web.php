<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('home', 'HomeController@index')->middleware('checklogin::class')->name('home');

Route::namespace('Admin')->prefix('category')->middleware('checklogin::class')->group(function () {
    Route::get('index', 'CategoryController@index')->name('category.index')->middleware('checkpermission:category-list');
    Route::get('create', 'CategoryController@create')->name('category.create')->middleware('checkpermission:category-create');
    Route::post('store', 'CategoryController@store')->name('category.store');
    Route::get('edit/{id}', 'CategoryController@edit')->name('category.edit')->middleware('checkpermission:category-edit');
    Route::post('update/{id}', 'CategoryController@update')->name('category.update');
    Route::get('destroy/{id}', 'CategoryController@destroy')->name('category.destroy')->middleware('checkpermission:category-delete');
});
Route::namespace('Admin')->prefix('permission')->middleware('checklogin::class')->group(function () {
    Route::get('index', 'PermissionController@index')->name('permission.index');
    Route::get('create', 'PermissionController@create')->name('permission.create');
    Route::post('store', 'PermissionController@store')->name('permission.store');
    Route::get('edit/{id}', 'PermissionController@edit')->name('permission.edit');
    Route::post('update/{id}', 'PermissionController@update')->name('permission.update');
    Route::get('destroy/{id}', 'PermissionController@destroy')->name('permission.destroy');
});
Route::namespace('Admin')->prefix('role')->middleware('checklogin::class')->group(function () {
    Route::get('index', 'RoleController@index')->name('role.index')->middleware('checkpermission:role-list');
    Route::get('create', 'RoleController@create')->name('role.create')->middleware('checkpermission:role-create');
    Route::post('store', 'RoleController@store')->name('role.store');
    Route::get('edit/{id}', 'RoleController@edit')->name('role.edit')->middleware('checkpermission:role-edit');
    Route::post('update/{id}', 'RoleController@update')->name('role.update');
    Route::get('destroy/{id}', 'RoleController@destroy')->name('role.destroy')->middleware('checkpermission:role-delete');
});
Route::namespace('Admin')->prefix('user')->middleware('checklogin::class')->group(function () {
    Route::get('index', 'UserController@index')->name('user.index')->middleware('checkpermission:user-list');
    Route::get('create', 'UserController@create')->name('user.create')->middleware('checkpermission:user-create');
    Route::post('store', 'UserController@store')->name('user.store');
    Route::get('edit/{id}', 'UserController@edit')->name('user.edit')->middleware('checkpermission:user-edit');
    Route::post('update/{id}', 'UserController@update')->name('user.update');
    Route::get('destroy/{id}', 'UserController@destroy')->name('user.destroy')->middleware('checkpermission:user-delete');
});

Route::namespace('Admin')->prefix('product')->middleware('checklogin::class')->group(function () {
    Route::get('index', 'ProductController@index')->name('product.index')->middleware('checkpermission:product-list');
    Route::post('store', 'ProductController@store')->name('product.store')->middleware('checkpermission:product-create');
    Route::get('edit/{id}', 'ProductController@edit')->name('product.edit')->middleware('checkpermission:product-edit');
    Route::put('update/{id}', 'ProductController@update')->name('product.update');
    Route::delete('destroy/{id}', 'ProductController@destroy')->name('product.destroy')->middleware('checkpermission:product-delete');
    Route::get('search', 'ProductController@search')->name('product.search');
});
