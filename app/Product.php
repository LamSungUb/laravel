<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Product extends Model
{
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function countProductInMonth()
    {
        $month = Carbon::now()->month;
        return $this->whereMonth('created_at', $month)->get()->count();
    }
}
