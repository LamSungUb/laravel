<?php

namespace App\Http\Controllers;

use App\Product;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    protected $product;
    protected $user;

    public function __construct(Product $product, User $user)
    {
        $this->middleware('auth');
        $this->product = $product;
        $this->user = $user;
    }

    public function index()
    {
        $countProductInMonth = $this->product->countProductInMonth();
        $countUserInMonth = $this->user->countUserInMonth();

        return view('home', [
            'countProductInMonth' => $countProductInMonth,
            'countUserInMonth' => $countUserInMonth
        ]);
    }
}
