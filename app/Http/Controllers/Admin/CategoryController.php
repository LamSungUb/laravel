<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Category\CreateProductRequest;
use App\Http\Requests\Admin\Category\UpdateProductRequest;

class CategoryController extends Controller
{
    protected $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function index()
    {
        $categories = $this->category->all();

        return view('category.index', [
            'categories' => $categories
        ]);
    }

    public function create()
    {
        return view('category.add');
    }

    public function store(CreateProductRequest $request)
    {
        $this->category->create($request->all());

        return redirect()->route('category.index')->with('success','successfully added');
    }

    public function edit($id)
    {
        $category = $this->category->findOrFail($id);

        return view('category.edit',[
            'category' => $category
        ]);
    }

    public function update(UpdateProductRequest $request, $id)
    {
        $this->category->findOrFail($id)->update($request->all());

        return redirect()->route('category.index')->with('success','repaired successfully');
    }

    public function destroy($id)
    {
        $this->category->destroy($id);

        return redirect()->route('category.index')->with('success','deleted successfully');
    }
}
