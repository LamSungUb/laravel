<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\CreateUserRequest;
use App\Http\Requests\Admin\User\UpdateUserRequest;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $role;

    protected $user;

    public function __construct(Role $role, User $user)
    {
        $this->role = $role;
        $this->user = $user;
    }

    public function index()
    {
        $users = $this->user->all();

        return view('user.index', [
            'users' => $users,
        ]);
    }

    public function create()
    {
        $roles = $this->role->all();

        return view('user.add',[
            'roles' => $roles
        ]);
    }

    public function store(CreateUserRequest $request)
    {
        $data = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
        ];
        $user = $this->user->create($data);

        $roleIds = $request->input('checkbox');
        $user->roles()->attach($roleIds);

        return redirect()->route('user.index')->with('success','successfully added');
    }

    public function edit($id)
    {
        $user = $this->user->findOrFail($id);
        $listRoleIds = $user->roles()->pluck('role_id');
        $roles = $this->role->all();

        return view('user.edit', [
            'user' => $user,
            'listRoleIds' => $listRoleIds,
            'roles' => $roles
        ]);
    }

    public function update(UpdateUserRequest $request, $id)
    {
        $data = [
            'name' => $request->input('name'),
        ];
        $roleIds = $request->input('checkbox');

        $user =  $this->user->findOrFail($id);
        $user->update($data);
        $user->roles()->detach();
        $user->roles()->attach($roleIds);

        return redirect()->route('user.index')->with('success','repaired successfully');
    }

    public function destroy($id)
    {
        $user = $this->user->findOrFail($id);
        $user->delete();
        $user->roles()->detach();

        return back()->with('success','deleted successfully');
    }
}
