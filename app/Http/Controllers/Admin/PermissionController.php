<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Permission\CreatePermissionRequest;
use App\Http\Requests\Admin\Permission\UpdatePermissionRequest;
use App\Permission;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    protected $permission;

    public function __construct(Permission $permission)
    {
        $this->permission = $permission;
    }

    public function index()
    {
        $permissions = $this->permission->all();

        return view('permission.index', [
            'permissions' => $permissions
        ]);
    }

    public function create()
    {
        return view('permission.add');
    }

    public function store(CreatePermissionRequest $request)
    {
        $this->permission->create($request->all());

        return redirect()->route('permission.index')->with('success','successfully added');
    }

    public function edit($id)
    {
        $permission = $this->permission->findOrFail($id);

        return view('permission.edit', [
            'permission' => $permission
        ]);
    }

    public function update(UpdatePermissionRequest $request, $id)
    {
        $this->permission->findOrFail($id)->update($request->all());

        return redirect()->route('permission.index')->with('success','repaired successfully');
    }

    public function destroy($id)
    {
        $this->permission->destroy($id);

        return redirect()->back()->with('success','deleted successfully');
    }
}
