<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Role\CreateRoleRequest;
use App\Http\Requests\Admin\Role\UpdateRoleRequest;
use App\Permission;
use Illuminate\Support\Facades\DB;
use App\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    protected $role;

    protected $permission;

    public function __construct(Role $role, Permission $permission)
    {
        $this->role = $role;
        $this->permission = $permission;
    }

    public function index()
    {
        $roles = $this->role->all();

        return view('role.index', [
            'roles' => $roles,
        ]);
    }

    public function create()
    {
        $permissions = $this->permission->all();

        return view('role.add', [
            'permissions' => $permissions
        ]);
    }

    public function store(CreateRoleRequest $request)
    {
        $data = [
            'name' => $request->input('name'),
            'display_name' => $request->input('display_name'),
        ];
        $permissionIds = $request->input('checkbox');
        $role = $this->role->create($data);
        $role->permissions()->attach($permissionIds);

        return redirect()->route('role.index')->with('success','successfully added');
    }

    public function edit($id)
    {
        $role = $this->role->findOrFail($id);
        $listPermissionIds = $role->permissions()->pluck('permission_id');
        $permissions = $this->permission->all();

        return view('role.edit', [
            'role' => $role,
            'listPermissionIds' => $listPermissionIds,
            'permissions' => $permissions
        ]);
    }

    public function update(UpdateRoleRequest $request, $id)
    {
        $data = [
            'name' => $request->input('name'),
            'display_name' => $request->input('display_name'),
        ];
        $permissionIds = $request->input('checkbox');
        $role = $this->role->findOrFail($id);
        $role->update($data);
        $role->permissions()->detach();
        $role->permissions()->attach($permissionIds);

        return redirect()->route('role.index')->with('success','repaired successfully');
    }


    public function destroy($id)
    {
        $role = $this->role->findOrFail($id);
        $role->delete();
        $role->users()->detach();
        $role->permissions()->detach();

        return back()->with('success','deleted successfully');
    }
}
