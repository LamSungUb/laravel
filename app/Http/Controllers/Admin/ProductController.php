<?php


namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Product\UpdateProductRequest;
use App\Http\Requests\Admin\Product\CreateProductRequest;
use App\Product;
use App\Service\ProductService;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected $product;

    protected $productService;

    public function __construct(Product $product, ProductService $productService)
    {
        $this->product = $product;
        $this->productService = $productService;
    }

    public function index()
    {
        $products = $this->product->all();

        return view('product.index', [
            'products' => $products
        ]);
    }

    public function store(CreateProductRequest $request)
    {
        $data = $request->all();
        $data['image'] = $request->hasFile('image') ? $this->productService->addImage($request->image) : $this->productService->defaultImageName;

        $this->product->create($data);

        return response()->json(["status" => 200]);
    }

    public function edit($id)
    {
        $product = $this->product->findOrFail($id);

        return view('product.modal_product_update', [
            'product' => $product,
        ]);
    }

    public function update(UpdateProductRequest $request, $id)
    {
        $data = $request->all();
        $product = $this->product->findOrFail($id);
        if ($request->hasFile('image')) {
            $this->productService->compareImage($product->image, $request->image);
            $data['image'] = $this->productService->addImage($request->image);
        }
        $product->update($data);

        return response()->json(["status" => 200]);
    }

    public function destroy($id)
    {
        $this->productService->deleteImage($id);
        $this->product->destroy($id);
        $products = $this->product->all();

        return view('product.table_product', [
            'products' => $products,
        ]);
    }

    public function search(Request $request)
    {
        $category_id = $request->get('category_id');
        $keyword = $request->get('keyword');
        $products = $this->productService->search($category_id, $keyword);

        return view('product.table_product', [
            'products' => $products,
        ]);
    }
}
