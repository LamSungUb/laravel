<div class="modal fade" id="modal_product_delete{{ $product->id }}" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <h5 class="text-center text-danger">Bạn chắc chắn muốn xóa</h5>
            </div>
            <div class="modal-footer">
                <button type="button" data-toggle="modal" data-target="#modal_product_delete{{ $product->id }}"
                        data-href="{{ route('product.destroy', $product->id) }}" class="btn-delete btn btn-primary">
                    Submit
                </button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
