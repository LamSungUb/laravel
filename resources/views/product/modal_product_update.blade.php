<div class="modal  modal-product-update" id="modal_product_update" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="form_update" enctype="multipart/form-data">
                {!! method_field('PUT') !!}
                <div class="modal-header">
                    <h5 class="modal-title">Update Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul id="errors_update"></ul>
                    <div class="row">
                        <div class="col-8 form-group">
                            <div>
                                <label>Name <sup class="text-danger">(*)</sup></label>
                                <input value="{{ $product->name }}" type="text" name="name" id="a" class="form-control">
                            </div>
                            <div>
                                <label>Price <sup class="text-danger">(*)</sup></label>
                                <input value="{{ $product->price }}" type="text" id="price" name="price"
                                       class="form-control">
                            </div>
                            <div>
                                <label>Category <sup class="text-danger">(*)</sup></label>
                                <select name="category_id" class="custom-select">
                                    <option value="{{ $product->category_id }}">{{ $product->category->name }}</option>
                                    @foreach($categories as $category)
                                        @if($category->id != $product->category_id)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Description<sup class="text-danger">(*)</sup></label>
                                <textarea name="description" class="form-control"
                                          rows="3">{{ $product->description }}</textarea>
                            </div>
                        </div>
                        <div class="col-4 form-group">
                            <div>
                                <label class="col-form-label">Image</label>
                                <div>
                                    <input type="file" accept="image/*" name="image" id="image_update"
                                           onchange="showImageUpdate(event)" style="display: none;">
                                    <img height="120" width="120" id="output1"
                                         src="{{ asset('Admin/images/'.$product->image) }}">
                                    <label class="form-label" for="image_update" style="cursor: pointer;">
                                        <p class="btn btn-outline-success">
                                            <i class="fa fa-upload"></i>
                                        </p>
                                    </label>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-href="{{ route('product.update',$product->id) }}"
                            class="btn btn-primary btn-update">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

