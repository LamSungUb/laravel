@extends('layouts.master')
@section('content')
    <!-- Topbar Search -->
    <nav class=" navbar-light bg-light">
        <form id="form_search">
            <div class="row">
                <div class="col-3">
                    <select id="category_id" class="form-control">
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-9">
                    <input class="form-control ml-sm-2" id="search" type="search" placeholder="Search" aria-label="Search" data-href="{{ route('product.search') }}">
                </div>
            </div>
        </form>
    </nav>
    <div class="table-responsive my-4" id="tbl_product_result">
        <table class="table table-hover">
            <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Product Name</th>
                <th scope="col">Image</th>
                <th scope="col">Price</th>
                <th scope="col">Category</th>
                <th scope="col">Description</th>
                <th scope="col">
                    <button type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#modal_product_create" id="btn_create">
                        Create
                    </button>
                    @include('product.modal_product_create')
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
                <tr>
                    <th scope="row">{{ $loop->iteration }}</th>
                    <th scope="col">{{ $product->name }}</th>
                    <th scope="col">
                        <img src="{{ asset('Admin/images/'.$product->image) }}" alt="" height="80" width="80">
                    </th>
                    <th scope="col">{{ $product->price }}</th>
                    <th scope="col">{{ $product->category->name }}</th>
                    <th scope="col">{{ $product->description }}</th>
                    <td>
                        <button type="button" data-toggle="modal" data-href="{{ route('product.edit',$product->id) }}"
                                data-id="{{ $product->id }}"
                                class="btn btn-success btn-edit btn-sm">Edit
                        </button>

                        <button type="button" data-toggle="modal" data-target="#modal_product_delete{{ $product->id }}"
                                class="btn btn-danger btn-sm">Delete
                        </button>
                        @include('product.modal_product_delete')
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
@section('script')
    <script src="product/js/product.js"></script>
@endsection
