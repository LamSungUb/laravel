@extends('layouts.master')
@section('content')
    @include('note')
    <div>
        <form method="POST" action="{{ route('user.store') }}">
            @csrf
            <div class="row form-group">
                <div class="col-6">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" name="name">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" name="email">
                    </div>
                    <div class="row">
                        <div class="col">
                            <label>Password</label>
                            <input type="password" class="form-control" name="password">
                        </div>
                        <div class="col">
                            <label>Password Confirmation</label>
                            <input type="password" class="form-control" name="password_confirmation">
                        </div>
                    </div>
                </div>
                <div class="form-group col-6">
                    <p>Role</p>
                    <div class="form-group form-inline">
                        @foreach($roles as $role)
                            <div class="custom-control custom-checkbox  mr-5">
                                <input type="checkbox" name="checkbox[]" value="{{ $role->id }}"
                                       class="custom-control-input" id="user{{ $role->id }}">
                                <label for="user{{ $role->id }}"
                                       class="custom-control-label">{{ $role->name }}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
