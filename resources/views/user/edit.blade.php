@extends('layouts.master')
@section('content')
    @include('note')
    <div>
        <form method="POST" action="{{ route('user.update',$user->id) }}">
            @csrf
            <div class="row form-group">
                <div class="col-6">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" value="{{ $user->email }}" name="email" readonly>
                    </div>
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" name="name" value="{{ $user->name }}">
                    </div>
                </div>
                <div class="form-group col-6">
                    <p>Role</p>
                    <div class="form-group form-inline">
                        @foreach($roles as $role)
                            <div class="custom-control custom-checkbox  mr-5">
                                <input type="checkbox" name="checkbox[]" value="{{ $role->id }}"
                                       {{ $listRoleIds->contains($role->id) ? "checked" : "" }}
                                       class="custom-control-input" id="user{{ $role->id }}">
                                <label for="user{{ $role->id }}"
                                       class="custom-control-label">{{ $role->name }}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
