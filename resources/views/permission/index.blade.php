@extends('layouts.master')
@section('content')
    @include('note')
    <div class="table-responsive">
        <table class="table table-hover">
            <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Permission Name</th>
                <th scope="col">Display Name</th>
                <th scope="col">
                    <a href="{{ route('permission.create') }}" type="button" class="btn btn-primary">Create</a>
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($permissions as $permission)
                <tr>
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>{{ $permission->name }}</td>
                    <td>{{ $permission->display_name }}</td>
                    <td>
                        <a href="{{ route('permission.edit',$permission->id) }}" type="button" class="btn btn-success btn-sm">Edit</a>
                        <a href="{{ route('permission.destroy',$permission->id) }}" type="button" class="btn btn-danger btn-sm">Delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
