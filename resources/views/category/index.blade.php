@extends('layouts.master')
@section('content')
    @include('note')
    <div class="table-responsive">
        <table class="table table-hover">
            <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Category Name</th>
                <th scope="col">
                    <a href="{{ route('category.create') }}" type="button" class="btn btn-primary">Create</a>
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
                <tr>
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>{{ $category->name }}</td>
                    <td>
                        <a href="{{ route('category.edit',$category->id) }}" type="button" class="btn btn-success btn-sm">Edit</a>
                        <a href="{{ route('category.destroy',$category->id) }}" type="button" class="btn btn-danger btn-sm">Delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
