@extends('layouts.master')
@section('content')
    @include('note')
    <div>
        <form method="POST" action="{{ route('category.store') }}">
            @csrf
            <div class="form-group">
                <label>Category Name</label>
                <input type="text" name="name" class="form-control" >
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
