@extends('layouts.master')
@section('content')
    @include('note')
    <div>
        <form method="POST" action="{{ route('role.update',$role->id) }}">
            @csrf
            <div class="form-group">
                <label>Permission Name</label>
                <input type="text" name="name" class="form-control" value="{{ $role->name }}">
            </div>
            <div class="form-group">
                <label>Display Name</label>
                <input type="text" name="display_name" class="form-control" value="{{ $role->display_name }}">
            </div>
            <div>
                <p>Permission</p>
                <div class="form-group form-inline">
                    @foreach($permissions as $permission)
                        <div class="custom-control custom-checkbox  mr-5">
                            <input type="checkbox"
                                   {{ $listPermissionIds->contains($permission->id) ? "checked" : "" }} name="checkbox[]"
                                   value="{{ $permission->id }}" class="custom-control-input"
                                   id="role{{ $permission->id }}">
                            <label class="custom-control-label"
                                   for="role{{ $permission->id }}">{{ $permission->name }}</label>
                        </div>
                    @endforeach
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
