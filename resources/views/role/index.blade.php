@extends('layouts.master')
@section('content')
    @include('note')
    <div class="table-responsive">
        <table class="table table-hover">
            <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Role Name</th>
                <th scope="col">Display Name</th>
                <th scope="col">Display Name Permission</th>
                <th scope="col">
                    <a href="{{ route('role.create') }}" type="button" class="btn btn-primary">Create</a>
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($roles as $role)
                <tr>
                    <th scope="row">{{ $role->iteration }}</th>
                    <td>{{ $role->name }}</td>
                    <td>{{ $role->display_name }}</td>
                    <td>
                        @foreach($role->permissions as $permission)
                            {{ $permission->display_name }},
                        @endforeach
                    </td>
                    <td>
                        <a href="{{ route('role.edit',$role->id) }}" type="button" class="btn btn-success btn-sm">Edit</a>
                        <a href="{{ route('role.destroy',$role->id) }}" type="button" class="btn btn-danger btn-sm">Delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
