@extends('layouts.master')
@section('content')
    @include('note')
    <div>
        <form method="POST" action="{{ route('role.store') }}">
            @csrf
            <div class="form-group">
                <label>Role Name</label>
                <input type="text" name="name" class="form-control">
            </div>
            <div class="form-group">
                <label>Display Name</label>
                <input type="text" name="display_name" class="form-control">
            </div>
            <div>
                <p>Permission</p>
                @foreach($permissions as $permission)
                    <div class="form-group">
                        <div class="custom-control custom-checkbox  mr-5">
                            <input type="checkbox" name="checkbox[]" value="{{ $permission->id }}"
                                   class="custom-control-input" id="role{{ $permission->id }}">
                            <label for="role{{ $permission->id }}"
                                   class="custom-control-label">{{ $permission->name }}</label>
                        </div>
                    </div>
                @endforeach
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
