// Show image
function showImage(event) {
    let image = document.getElementById('output');
    image.src = URL.createObjectURL(event.target.files[0]);
};

function showImageUpdate(event) {
    let image = document.getElementById('output1');
    image.src = URL.createObjectURL(event.target.files[0]);
};


//function ajaxSetup
function ajaxSetup() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });
}

//function ajaxForm
function callAjaxForm(url, method, data) {
    return $.ajax({
        data: data,
        dataType: 'json',
        method: method,
        contentType: false,
        processData: false,
        url: url
    })
}

//function ajax
function callAjax(url, type, data) {
    return $.ajax({
        data: data,
        dataType: 'html',
        type: type,
        url: url
    })
}
